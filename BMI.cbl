       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. MUKKU.
       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01  BMI      PIC   9(4)V99  VALUE ZEROS.
       01  HEIGHT   PIC   9(4)V99  VALUE ZEROS.
       01  WEIGHT   PIC   9(4)V99  VALUE ZEROS.
       01  MESSAGES  PIC   X(100)   VALUE SPACE.
        88 UNDERWEIGHT    VALUE "UNDERWEIGHT => BMI LESS THAN 18.5".
        88 NORMAL         VALUE "NORMAL => BMI BETWEEN 18.5 AND 24.9".
        88 OVERWEIGHT     VALUE "OVERWEIGHT => BMI BETWEEN 25 AND 29.9".
        88 OBESE          VALUE "OBESE => BMI BETWEEN 30 AND 34.9".
        88 EXTREMLY-OBESE VALUE "EXTREMLY-OBESE => BMI GREATER THAN 35".
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "ENTER YOUR HEIGHT (cm.) : " WITH NO ADVANCING 
           ACCEPT HEIGHT
           DISPLAY "ENTER YOUR WEIGHT (kg.) : " WITH NO ADVANCING
           ACCEPT WEIGHT
           COMPUTE BMI = WEIGHT / (HEIGHT/100)**2
           IF BMI < 18.5 THEN 
              SET UNDERWEIGHT TO TRUE
           END-IF
           IF BMI >= 18.5 AND BMI <= 24.9 THEN 
              SET NORMAL TO TRUE
           END-IF
           IF BMI >= 25 AND BMI <= 29.9 THEN 
              SET OVERWEIGHT TO TRUE
           END-IF
           IF BMI >= 30 AND BMI <= 34.9 THEN 
              SET OBESE TO TRUE
           END-IF
           IF BMI >= 35 THEN 
              SET EXTREMLY-OBESE TO TRUE
           END-IF
           DISPLAY "=================================================="
           DISPLAY "HEIGHT : " HEIGHT " cm."
           DISPLAY "WEIGHT : " WEIGHT " kg."
           DISPLAY "------------------------------------"
           DISPLAY "BMI : " BMI
           DISPLAY MESSAGES WITH NO ADVANCING 
           GOBACK 
           .